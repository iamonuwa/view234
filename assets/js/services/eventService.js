/**
 * Created by su on 1/6/17.
 */

angular.module('eventService', []).factory('eventData', function ($http, API, $q, auth) {

    var INSTAGRAM = "https://api.instagram.com/v1";
    var client_id = "7fdd032b887b4a8b8526f699b474b5b9";

    var config = {
        headers : {
            'Content-Type': 'application/json'
        }
    };

    var params = {
        'client_id': client_id,
        'count': '5',
        'callback': 'JSON_CALLBACK'
    };

    self.getEvents = function () {
        var defObj = $q.defer();
        $http.get(API + 'events')
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    }

    self.getEvent = function (id) {
        var defObj = $q.defer();
        $http.get(API+'events/'+id)
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    }

    self.createNewEvent = function (credentials) {
            return $http({
                method: 'POST',
                url: API + 'user/events?token='+ auth.getToken(),
                data: credentials,
                headers: {
                    'Accept': 'application/json',
                    'content-type': 'application/json'
                }
            });
        };

    self.storeImage = function (data) {
        var file = {
            promo_image: data
        }
        return $http({
          method: 'POST',
          url: API + 'user/events/image?token='+ auth.getToken(),
          params: file,
          headers : { 'Content-Type' : undefined }, 
          transformRequest : angular.identity 
        })
    }


    self.getMyEvents = function () {
        var defObj = $q.defer();
        $http.get(API+'user/events?token='+ auth.getToken())
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    };

    self.getMyEvent = function (id) {
        var defObj = $q.defer();
        $http.get(API+'user/events/'+id+'?token='+ auth.getToken())
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    };

    self.deleteEvent = function (id) {
        return $http({
                method: 'POST',
                url: API + 'user/events/'+ id +'/delete',
                params: {
                    token: auth.getToken()
                },
            });
    }

    self.updateEvent = function (credentials) {
            return $http({
                method: 'POST',
                url: API + 'user/events/'+ credentials.event +'/update?token='+ auth.getToken(),
                params: {
                        name: credentials.name,
                        location: credentials.location,
                        type: credentials.type,
                        price: credentials.price,
                        date: credentials.date,
                        time: credentials.time,
                        description: credentials.description,
                        twitter_hashtag: credentials.twitter_hashtag,
                        promo_image_url: credentials.promo_image_url,
                        facebook_hashtag: credentials.twitter_hashtag,
                        youtube_live_stream_url : credentials.youtube_live_stream_url,
                        download_link: credentials.download_link
                                },
                headers: {'Content-Type': 'application/json'}
            });
    },

    self.instagram = function(callback) {
        var endpoint = 'https://api.instagram.com/v1/users/';
        endpoint += user_id;
        endpoint += '/media/recent/?';
        endpoint += '?count=99';
        endpoint += '&callback=JSON_CALLBACK';
        endpoint += '&access_token=' + access_token;
        $http.jsonp(endpoint)
        .success(function(response) {
          callback(response.data);
        })
        .error(function(xhr, status, err) {
          console.error(status, err);
        })
      };

      self.fetchPopular = function(callback){
        var endpoint = "https://api.instagram.com/v1/media/popular";
        endpoint += "?count=99";
        endpoint += "&client_id=" + client_id;
        endpoint += "&callback=JSON_CALLBACK";
        $http.jsonp(endpoint).success(function(response){
          callback(response.data);
        });
    }

    return {
        createNewEvent: self.createNewEvent,
        getEvents: self.getEvents,
        getMyEvents: self.getMyEvents,
        getMyEvent: self.getMyEvent,
        getEvent: self.getEvent,
        storeImage: self.storeImage,
        deleteEvent: self.deleteEvent,
        updateEvent: self.updateEvent,
        instagram: self.instagram
    }
});
