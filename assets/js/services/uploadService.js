var uploadService = angular.module('uploadService', [])

uploadService.factory('upload', function($http, $q, API){
  return function(content){
    var defObj = $q.defer()
    $http.post(API + 'upload/content', content, {headers: {'Content-Type': 'application/json'}} )
      .success(function(data){
        defObj.resolve({
          data : data
        })
      })
      .error(function(e){
        defObj.resolve({
          data : e
        })
      })
      return defObj.promise
    }
})
