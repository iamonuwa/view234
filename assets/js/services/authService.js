/**
 * Created by su on 1/4/17.
 */

angular.module('authService', []).factory('auth', function ($window) {
    var self = this;
    self.parseJwt= function (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse($window.atob(base64));
    };
    self.saveToken = function (token) {
        $window.localStorage['234ViewToken'] = token;
    };
    self.getToken = function () {
        return $window.localStorage['234ViewToken'];
    };
    self.isAuthed = function () {
        var token = self.getToken();
        if (token) {
            var params = self.parseJwt(token);
            return Math.round(new Date().getTime() / 1000) <= params.exp;
        } else {
            return false;
        }
    };
    self.getEmail = function () {
        return $window.localStorage['234ViewData'];
    }
    self.logout = function () {
        $window.localStorage.removeItem('234ViewToken');
        $window.localStorage.removeItem('234ViewData');
    };

    self.saveData = function (data) {
        $window.localStorage['234ViewData'] = data
    }

    self.getUser = function () {
        return $window.localStorage['234ViewData'];
    }
    return {
        parseJwt : self.parseJwt,
        saveToken: self.saveToken,
        getToken : self.getToken,
        isAuthed : self.isAuthed,
        logout   : self.logout,
        saveData : self.saveData,
        getUser: self.getUser,
        getEmail: self.getEmail,
    }
});