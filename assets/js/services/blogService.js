/**
 * Created by su on 1/22/17.
 */
var blogService = angular.module('blogService', [])


angular.module('blogService', []).factory('blogPosts', function ($http, $q) {
    var self = this;

    self.getPosts = function () {
        var defObj = $q.defer();
        $http.get('http://crystaldigest.com/wp-json/posts')
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    };

    self.getPost = function (id) {
        var defObj = $q.defer()
        $http.get('http://crystaldigest.com/wp-json/posts/'+id)
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    };

    return {
        getPosts: self.getPosts,
        getPost: self.getPost
    }


});


