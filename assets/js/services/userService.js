/**
 * Created by su on 1/6/17.
 */

angular.module('userService', []).factory('account', function ($http, API, $q, auth) {

    $http.defaults.useXDomain = true;

    self.register = function(credentials) {
        return $http({
                method: 'POST',
                url: API + 'register',
                data: credentials,
                headers: {
                    'Accept': 'application/json',
                    'content-type': 'application/json'
                }
            });
    };

    self.login = function (credentials) {
        return $http({
                method: 'POST',
                url: API + 'login',
                data: credentials,
                headers: {
                    'Accept': 'application/json',
                    'content-type': 'application/json'
                }
            });
    };
    
    self.getUser = function () {
        return auth.getUser();
    };

    self.getSingleUser = function (id) {
        var defObj = $q.defer();
        $http.get(API + 'admin/users/'+id+'?token='+auth.getToken())
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    }
    
    self.getUsers = function () {
        var defObj = $q.defer();
        $http.get(API + 'admin/users?token='+auth.getToken())
            .success(function(data){
                defObj.resolve({
                    data : data
                })
            })
            .error(function(e){
                defObj.resolve({
                    data : false
                })
            })
        return defObj.promise
    }

    self.unbanUser = function (id) {
        return $http({
                method: 'POST',
                url: API + 'admin/users/' + id + '/enable?token=' + auth.getToken()
            })
    }

    self.banUser = function (id) {
        return $http({
                method: 'POST',
                url: API + 'admin/users/' + id + '/disable?token=' + auth.getToken()
            })
    }



    return {
        register: self.register,
        login: self.login,
        getUser: self.getUser,
        getUsers: self.getUsers,
        getSingleUser: self.getSingleUser,
        unbanUser: self.unbanUser,
        banUser: self.banUser
    }
});
