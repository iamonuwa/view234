var homeService = angular.module('homeService', [])

homeService.factory('getContent', function($http, $q, API){
  return function(contentName){
    var defObj = $q.defer()
    $http.get('https://videodataapi.herokuapp.com/v1.0/' + contentName)
      .success(function(data){
        defObj.resolve({
          data : data
        })
      })
      .error(function(e){
        defObj.resolve({
          data : false
        })
      })
      return defObj.promise
    }
})


