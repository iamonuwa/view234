var app = angular.module('app', ['satellizer', 'ngRoute','ui.router','toastr', 'chieffancypants.loadingBar','truncate', 'homeModule', 'videoModule', 'packageModule', 'eventManagerModule', 'userManagerModule','priceModule', 'upload', 'event','datatables','blogModule','moment-picker','ngtweet','ngMap','disqusHere','angularUtils.directives.dirDisqus','ngSanitize', 'ngFileUpload'])

app.config(function($routeProvider, $locationProvider, $authProvider, $httpProvider){
  $routeProvider
    .when('/home', {
      templateUrl: 'partials/home.html',
      controller: 'HomeCtrl'
    })
    .when('/pricing', {
      templateUrl: 'partials/pricing.html',
        controller: 'PriceCtrl'
    })
    .when('/events', {
      templateUrl : 'partials/events.html',
      controller: 'EventCtrl'
    })
    .when('/event/:id', {
      templateUrl : 'partials/event.html',
      controller: 'EventCtrl'
    })
    .when('/digitaltv', {
      templateUrl : 'partials/home.html',
      controller: 'HomeCtrl'
    })
    .when('/blog', {
      templateUrl : 'partials/blog.html',
      controller: 'BlogCtrl'
    })
    .when('/blog/:slug',{
        templateUrl: 'partials/readBlog.html',
        controller: 'BlogCtrl'
    })
    .when('/article', {
      templateUrl : 'partials/article.html'
    })
    .when('/trending', {
      templateUrl : 'partials/home.html',
      controller: 'HomeCtrl'
    })
    .when('/statistics', {
      templateUrl: 'partials/statistics.html',
      controller: 'EventManagerCtrl'
    })
    .when('/more', {
      templateUrl : 'partials/about.html'
    })
    .when('/about', {
      templateUrl : 'partials/about.html'
    })
    .when('/contact', {
      templateUrl : 'partials/contact.html'
    })
    .when('/terms', {
      templateUrl : 'partials/terms.html'
    })
    .when('/channel', {
      templateUrl: 'partials/channel.html',
      controller: 'HomeCtrl'
    })
    .when('/search/:id', {
      templateUrl: 'partials/searchPage.html',
      controller: 'HomeCtrl'
    })
    .when('/video/:id', {
      templateUrl: 'partials/video.html',
      controller: 'VideoCtrl'
    })
    .when('/live_stream', {
      templateUrl : 'partials/live_stream.html'
    })
    .when('/upload', {
      templateUrl: 'partials/upload.html',
      controller: 'UploadCtrl',
    })
    .when('/manage_packages', {
      templateUrl : 'partials/manage_packages.html',
      controller: 'PackageCtrl'
    })
    .when('/manage_feeds', {
      templateUrl: 'partials/manage_feeds.html'
    })
    .when('/manage-events', {
      templateUrl: 'partials/manage_events.html',
      controller: 'EventManagerCtrl'
    })
    .when('/manage-events/create', {
        templateUrl: 'partials/create_new_event.html',
        controller: 'EventManagerCtrl'
    })
    .when('/manage-events/:event', {
        templateUrl: 'partials/edit_event.html',
        controller: 'EventManagerCtrl'
    })
    .when('/manage-events/:event/banner', {
        templateUrl: 'partials/event_banner.html',
        controller: 'EventManagerCtrl'
    })
    .when('/manage-users', {
      templateUrl : 'partials/manage_users.html',
      controller: 'UserManagerCtrl'
    })
    .when('/manage-users/create', {
      templateUrl : 'partials/new_user.html',
      controller: 'UserManagerCtrl'
    })
    .when('/manage-users/:id', {
      templateUrl : 'partials/edit_user.html',
      controller: 'UserManagerCtrl'
    })
    .otherwise({redirectTo: '/home'})

  // $locationProvider.html5Mode(true);

      // $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];


    $authProvider.twitter({
      url: '/auth/twitter',
      authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
      redirectUri: window.location.origin,
      oauthType: '1.0',
      popupOptions: { width: 495, height: 645 }
    });

})

app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);

// app.config(['$sceDelegateProvider', function($sceDelegateProvider) {
//     $sceDelegateProvider.resourceUrlWhitelist([
//         'self',
//         'http://35.165.38.70/api/v1/**'
//     ]);
// }]);

// app.constant('API', 'http://35.165.38.70/api/v1/')
app.constant('API', 'https://view234.herokuapp.com/api/v1/')

app.filter('trustUrl', function ($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url);
    }
  })

app.filter('escape', function() {
    return function(input) {
        if(input) {
            return window.encodeURIComponent(input)
        }
        return ""
    }
});

app.run(['$rootScope','$location', 'auth','account', function($rootScope, $location, auth, account) {

    $rootScope.$watch(function() { return account.getUser() },
        function(newValue, oldValue) {
            $rootScope.auth = auth.isAuthed();
            if(auth.isAuthed()){
            $rootScope.email = JSON.parse(newValue).email;
            if((JSON.parse(newValue).roles).length > 0){
                if(JSON.parse(newValue).roles[0].slug === 'admin'){
                   $rootScope.role = true;
                 }
                else{
                  $rootScope.role = false;
                 }
             }
             else{
              $rootScope.role = false;
             }
          }
        }
    );

    $rootScope.redirectPath = null;
    $rootScope.$on('$routeUpdate', function(event, next, current) {
        $rootScope.activeFeed = next.params.feed || false;
        next.scope && (next.scope.activeFeed = next.params.feed||false);
    });

    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if (next.authRequired && !auth.isAuthed()) {
            $location.path("/auth");
        }
    });

    $rootScope.$on('$stateChangeStart', function(event, newState) {
        if (!Auth.isLoggedIn() && newState.authRequired != true) {
            $location.path("/auth");
        }
    });
}])

app.filter('htmlToPlaintext', function()
{
    return function(text)
    {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});


function authInterceptor(API, auth) {
    return {
        request: function (config) {
            var token = auth.getToken();
            if (config.url.indexOf(API) === 0 && token) {
                config.headers.Authorization = 'Bearer ' + token;
            }
            return config;
        },
        response: function (res) {
            if (res.config.url.indexOf(API) === 0 && res.data.token) {
                auth.saveToken(res.data.token);
            }

            return res;
        }
    }
}
app.run(initDT);
function initDT(DTDefaultOptions) {
    DTDefaultOptions.setLoadingTemplate('<img src="images/loading.gif">');
}
