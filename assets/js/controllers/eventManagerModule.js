var eventManagerModule = angular.module('eventManagerModule', ['ui.bootstrap','eventService','authService'])

eventManagerModule.controller('EventManagerCtrl', function($scope, $document, $location, $uibModal, eventData, auth, $route, toastr, $routeParams, NgMap, $timeout, $http , $compile, $upload, API) {

    var url = API + 'events/image';

    $scope.showPrice = false;

    if (auth.isAuthed()){
        $scope.events = function () {
            eventData.getMyEvents().then(function (result) {
                $scope.events = result.data.data.data;
            });
        }
    }
    else{
        $location.path('home');
    }

    $scope.upload_stats = function () {
      $scope.upload_stats = $scope.events.length;
      $scope.upload_likes = 0;
      $scope.upload_views = 0;
    }

    $scope.openEvent = function (id) {
      $location.path('/manage-events/'+id)
    };

    $scope.getSingleEvent = function (id) {
      eventData.getMyEvent($routeParams.event).then(function(result){
        $scope.item  = result.data.data;
      })
    };


$scope.createNewEvent = function (files) {
  
      $scope.formUpload = true;
        if (files != null) {
          generateThumbAndUpload(files[0])
        }
  
      function generateThumbAndUpload(file) {
        $scope.errorMsg = null;
        $scope.generateThumb(file);
        uploadUsing$upload(file);
      }
  
      $scope.generateThumb = function(file) {
        if (file != null) {
          if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
            $timeout(function() {
              var fileReader = new FileReader();
              fileReader.readAsDataURL(file);
              fileReader.onload = function(e) {
                $timeout(function() {
                  file.dataUrl = e.target.result;
                });
              }
            });
          }
        }
      }
      function uploadUsing$upload(file) {
        file.upload = $upload.upload({
          url: url + $scope.getReqParams(),
          method: 'POST',
          sendObjectsAsJsonBlob: false,
          file: file,
          fileFormDataName: 'file',
        });

      file.upload.then(function(response) {
        $timeout(function() {
          file.result = response.data;
          $scope.uploadedFiles = response.data;

          var data = {
           name: $scope.event.name,
            location: $scope.event.location,
            description: $scope.event.description,
            date: $scope.event.date,
            time: $scope.event.time,
            type: $scope.event.type,
            price: $scope.event.price,
            ticket: $scope.event.ticket,
            promo_image_url: response.data.data.url,
            facebook_hashtag: $scope.event.facebook,
            twitter_hashtag: $scope.event.twitter,
            youtube_live_stream_url: $scope.event.livestream,
            download_link: $scope.download_link
        }; 
          eventData.createNewEvent(data).then(function (response) {
            $location.path('/manage-events');
            toastr.success('Event has been has been created successfully', 'Success', {timeout: 3000});
          })

        });
    }, function(response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    });
  }
  $scope.getReqParams = function() {
    return $scope.generateErrorOnServer ? "?errorCode=" + $scope.serverErrorCode + 
        "&errorMessage=" + $scope.serverErrorMsg : "";
  }

}

    $scope.updateEvent = function (files) {
  
      $scope.formUpload = true;
        if (files != null) {
          generateThumbAndUpload(files[0])
        }
  
      function generateThumbAndUpload(file) {
        $scope.errorMsg = null;
        $scope.generateThumb(file);
        uploadUsing$upload(file);
      }
  
      $scope.generateThumb = function(file) {
        if (file != null) {
          if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
            $timeout(function() {
              var fileReader = new FileReader();
              fileReader.readAsDataURL(file);
              fileReader.onload = function(e) {
                $timeout(function() {
                  file.dataUrl = e.target.result;
                });
              }
            });
          }
        }
      }
      function uploadUsing$upload(file) {
        file.upload = $upload.upload({
          url: url + $scope.getReqParams(),
          method: 'POST',
          sendObjectsAsJsonBlob: false,
          file: file,
          fileFormDataName: 'file',
        });

      file.upload.then(function(response) {
        $timeout(function() {
          file.result = response.data;
          $scope.uploadedFiles = response.data;

          var data = {
            event: $routeParams.event,
            name: $scope.event.name,
            location: $scope.event.location,
            description: $scope.event.description,
            date: $scope.event.date,
            time: $scope.event.time,
            type: $scope.event.type,
            price: $scope.event.price,
            ticket: $scope.event.ticket,
            promo_image_url: response.data.data.url,
            facebook_hashtag: $scope.event.facebook,
            twitter_hashtag: $scope.event.twitter,
            youtube_live_stream_url: $scope.event.livestream,
            download_link: $scope.download_link
        }; 
          eventData.updateEvent(data).then(function (response) {
            $location.path('/manage-events');
            toastr.success('Event has been has been created successfully', 'Success', {timeout: 3000});
          })

        });
    }, function(response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    });
  }
  $scope.getReqParams = function() {
    // return $scope.generateErrorOnServer ? "?errorCode=" + $scope.serverErrorCode + 
    //     "&errorMessage=" + $scope.serverErrorMsg : "";
        toastr.error($scope.serverErrorMsg, 'Error', {timeout: 3000});
  }
}

    $scope.attachFile = function (id) {
      $location.path('manage-events/'+id+'/banner');
    }
  $scope.open = {

    openModal : function(template){
      var parentElem = $document.find('body');
      $uibModal.open({
          animation: true,
          templateUrl: 'partials/' + template + '.html',
          size: 'lg',
          appendTo: parentElem,
          scope: $scope,
      })
    },

    init : function(template, id) {
      if(id == null){
        this.openModal(template)
      }
      else{
          $scope.eventId = id;
          $scope.eventData = $scope.events.filter(function(elt){
              return elt.id == id
          })[0];
          $scope.newdata = $scope.eventData;
          this.openModal(template)
      }
    },

    save : function(id){
      for (i = 0; i < $scope.events.length; i++){
        if ($scope.events[i].id == id){
          $scope.events[i] = $scope.newdata
        }
      }
      return true
    },

    delete : function (id) {
        eventData.deleteEvent(id)
            .then(function (res) {
                toastr.success("Event has been removed. ", "Success", {timeout: 3000});
                $route.reload();
            });
    }
  }


  var vm = this;

  $scope.type = 'address';

  // vm.placeChanged = function() {
  //   vm.place = this.getPlace();
  //   vm.map.setCenter(vm.place.geometry.location);
  // }
  NgMap.getMap().then(function(map) {
    vm.map = map;
  });
    $scope.uploadedFiles = [];
  $scope.$watch('files', function(files) {
    $scope.formUpload = false;
    if (files != null) {
      for (var i = 0; i < files.length; i++) {
        $scope.errorMsg = null;
        (function(file) {
          uploadUsing$upload(file);
        })(files[i]);
      }
    }
  });
  
  $scope.uploadPic = function(files) {
    $scope.formUpload = true;
    if (files != null) {
      generateThumbAndUpload(files[0])
    }
  }
  
  function generateThumbAndUpload(file) {
    $scope.errorMsg = null;
    $scope.generateThumb(file);
    uploadUsing$upload(file);
  }
  
  $scope.generateThumb = function(file) {
    if (file != null) {
      if ($scope.fileReaderSupported && file.type.indexOf('image') > -1) {
        $timeout(function() {
          var fileReader = new FileReader();
          fileReader.readAsDataURL(file);
          fileReader.onload = function(e) {
            $timeout(function() {
              file.dataUrl = e.target.result;
            });
          }
        });
      }
    }
  }
  function uploadUsing$upload(file) {
    file.upload = $upload.upload({
      url: url + $scope.getReqParams(),
      method: 'POST',
      sendObjectsAsJsonBlob: false,
      file: file,
      fileFormDataName: 'file',
    });

    file.upload.then(function(response) {
        $timeout(function() {
          file.result = response.data;
          $scope.uploadedFiles = response.data;
          var credentials = {
            event: $routeParams.event,
            promo_image_url: response.data.data.url
          }    
          eventData.updateEvent(credentials).then(function (result) {
            if(result.data.success === true){
              toastr.success("Your event banner has been uploaded.");
            }
            else{
              toastr.error("An error has occured. Please try again");
            }
          })
        });
    }, function(response) {
      if (response.status > 0)
        $scope.errorMsg = response.status + ': ' + response.data;
    });

    file.upload.progress(function(evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });

    file.upload.xhr(function(xhr) {
      // xhr.upload.addEventListener('abort', function(){console.log('abort complete')}, false);
    });
  }
  $scope.getReqParams = function() {
    return $scope.generateErrorOnServer ? "?errorCode=" + $scope.serverErrorCode + 
        "&errorMessage=" + $scope.serverErrorMsg : "";
  }

  window.addEventListener("dragover", function(e) {
    e.preventDefault();
  }, false);
  window.addEventListener("drop", function(e) {
    e.preventDefault();
  }, false);



})

