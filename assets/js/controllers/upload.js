var upload = angular.module('upload', ['uploadService', 'textAngular', 'ui.bootstrap'])

upload.config(function ($httpProvider) {
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
});

upload.controller('UploadCtrl', function($scope, $document, $uibModal, upload) {
  $scope.uploadLinkData = {}

  $scope.modal = {
    heading : '',
    body : '',
    openModal : function(){
      var parentElem = $document.find('body')
      $uibModal.open({
        templateUrl: 'partials/uploadModal.html',
        appendTo: parentElem,
        scope: $scope
      })
    }
  }

  $scope.uploadData = function(data, content){
    // if (Object.keys($scope.uploadLinkData).length !== 10) {
    //   return false
    // }
    $scope.uploadLinkData.content = content
    data = JSON.stringify(data)
    upload(data).then(function(result){
      $scope.uploadMessage  = result.data


      if ($scope.uploadMessage && $scope.uploadMessage.success){
        $scope.modal.heading = 'Upload successful'
        $scope.modal.body = 'Thanks for uploading...'

        $scope.modal.openModal()
        $scope.uploadLinkData = {}
        content = ''
      }
      else{
        $scope.modal.heading = 'Upload unsuccessful'
        $scope.modal.body = 'Please, try again...'

        $scope.modal.openModal()
      }

    })
  }

})
