var homeModule = angular.module('homeModule', ['ngAnimate', 'ui.bootstrap', 'homeService','authService','userService'])

homeModule.controller('HomeCtrl', function($scope, $document, $uibModal, $rootScope, $location, $routeParams, $route, $state, toastr, getContent, account, auth){

  var self = this;
  var error = [];

  $scope.getData = function(){
    getContent($location.path().slice(1)).then(function(result){
      $scope.data = result.data
    })
  };

  function sessionStore(method, data){
    if (method === 'set')
      sessionStorage.setItem('navCurrent', data);
    else return sessionStorage.getItem('navCurrent')
  }

    function handleRequest (res) {
        if(res.data.error){
            toastr.error(res.data.error + "\n", "Authentication Failure", {timeout: 3000});
        }
        else{
            var token = res.data ? res.data.data.token : null;
            var user = JSON.stringify(res.data.data.user);
            if(token){
                auth.saveToken(token);
                auth.saveData(user);
                toastr.success("Welcome "+ res.data.data.user.name);
            }
        }
    }

    function isLoggedin() {
        return auth.isAuthed() ? auth.isAuthed() : false
    }

  $scope.switch = {
    current: sessionStore('', '') || 'Home',
    home: 'Home',
    events: 'Events',
    digitaltv: 'Digital TV',
    blog: 'Blog',
    trending: 'Trending',
    search: 'Search',
    upload: 'Upload',
    more: 'More',
    isCollapsed : true,
    switcher : function(switchVar){
      sessionStore('set', switchVar);
      this.current = sessionStore('', switchVar) || switchVar
    }
  };

  //Logged in user only

  $scope.user = {
      auth: '',
      type : 'standard',

    authenticate: function(email, password){
          var credentials = {
              email: email,
              password: password
          }
        account.login(credentials)
            .then(handleRequest, handleRequest)
    },

    createUser: function (username, email, password, cpassword) {
        var credentials = {
            name: username,
            email: email,
            password: password,
            password_confirmation: cpassword,
            role: 'admin'
        }
        account.register(credentials)
            .then(handleRequest, handleRequest)
    },

    logout: function () {
        auth.logout && auth.logout();
    }


  };

    $scope.$watch(function() { return isLoggedin() },
        function(newValue, oldValue) {
           $scope.user.auth = newValue;
        }
    );


  $scope.searchOptions = {
    query: '',
    digitaltv: true,
    blog: true,
    channels: true,
    search : function(){
      var value = self.query || $routeParams.id
      getContent(value).then(function(result){
        $scope.data = result.data
        console.log($scope.data)
        console.log(value)
      })
    }
  }

  $scope.followOptions = {
    text : 'Follow channel',
    follow : function(){
      this.text = this.text == 'Follow channel' ? 'Following' : 'Follow channel'
    }
  }

  $scope.channel = {
    username : 'channel username',
    desc : 'Channel description, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',

    save: function(auth){
      return true
    }
  }

  $scope.openModal = {
    open : function (template) {
      var parentElem = $document.find('body');
      $uibModal.open({
          templateUrl: 'partials/' + template + '.html',
          appendTo: parentElem,
          controller: 'HomeCtrl',
          scope: $scope
      })
    }
  };
  $scope.cancel = function () {
    $uibModal.dismiss('cancel');
  }
});
