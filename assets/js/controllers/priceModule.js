/**
 * Created by su on 1/22/17.
 */
var priceModule = angular.module('priceModule', ['ui.bootstrap', 'authService'])

priceModule.controller('PriceCtrl', function ($scope, toastr, $rootScope, auth) {

        $scope.paystack = function (amount) {
            if($rootScope.auth){
            var handler = PaystackPop.setup({
                key: 'pk_test_2da80a7ebd7b680aad762bfda430fbeb0b957446',
                email: $rootScope.email,
                amount: amount,
                metadata: {
                    custom_fields: [
                        {
                            display_name: "Mobile Number",
                            variable_name: "mobile_number",
                            value: "+2348062457326"
                        }
                    ]
                },
                callback: function(response){
                    alert('success. transaction ref is ' + response.reference);
                },
                onClose: function(){
                    // alert('window closed');
                }
            });

            handler.openIframe();
        }
            else{
                toastr.error("You must be logged in to make payments.", "Error", {timeout: 3000});
            }
    }



        openModal = function(template){
            var parentElem = $document.find('body')
            $uibModal.open({
                templateUrl: 'partials/' + template + '.html',
                appendTo: parentElem,
                scope: $scope
            })
        }
})