var event = angular.module('event', ['ui.bootstrap', 'videoService', 'eventService'])


event.controller('EventCtrl', function($scope, $document, $uibModal, $routeParams, $location, getVideo, $rootScope, eventData) {
    $scope.getEvents = function () {
        eventData.getEvents().then(function (result) {
            $scope.items = result.data.data.data;
        });
    };

    // $scope.getEvent = function () {
    //     console.log("ID: "+ $routeParams.id);
    // }


  $scope.fetchPopular = function(data){
      $scope.pics = data;
      console.log(JSON.stringify(data));
    };

  $scope.likeOptions = {
    like : function(likeCount){
      likes = parseInt(likeCount) + 1;
      $scope.videoData.likes = likes
    }
  };

  $scope.commentOptions = {
    showCommentBox : false,
    newComment : "",

    addComment : function(){
      $scope.videoData.commentsArr.push({
        user : 'user' + Math.ceil(Math.random(3, 100)),
        comment : this.newComment
      });
      this.newComment = "";
      this.showCommentBox = !this.showCommentBox
    }
  };

  $scope.followOptions = {
    text : 'Follow channel',
    follow : function(){
      this.text = this.text == 'Follow channel' ? 'Following' : 'Follow channel'
    }
  };

  $scope.getEventData = function(){
    eventData.getEvent($routeParams.id).then(function(result){
      $scope.item  = result.data.data;
      $scope.address = result.data.data.location;
      if($scope.item.type == 'paid'){
        $scope.type = true;
      }
      else{
        $scope.type = false;
      }
      $scope.price = '3500000';
      $scope.currentUrl = $location.absUrl();
    })
  }


  $scope.resfeshIframe = function() { 
    var iFrame = $document.find("anIframe");
    iFrame.attr("src",iFrame.attr("src"));
};

    $scope.paystack = function (amount) {
        var handler = PaystackPop.setup({
            key: 'pk_test_2da80a7ebd7b680aad762bfda430fbeb0b957446',
            email: $rootScope.email,
            amount: amount,
            metadata: {
                custom_fields: [
                    {
                        display_name: "Mobile Number",
                        variable_name: "mobile_number",
                        value: "+2348062457326"
                    }
                ]
            },
            callback: function(response){
                alert('success. transaction ref is ' + response.reference);
            },
            onClose: function(){
                // alert('window closed');
            }
        });

        handler.openIframe();
    }

});
