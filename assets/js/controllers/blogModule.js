/**
 * Created by su on 1/22/17.
 */
var blogModule = angular.module('blogModule', ['ngAnimate', 'ui.bootstrap', 'blogService'])

blogModule.controller('BlogCtrl',['$scope', '$rootScope', '$routeParams','$state', '$location','blogPosts', function($scope, $rootScope, $routeParams, $state, $location, blogPosts){



    $scope.getData = function(){
        blogPosts.getPosts($location.path().slice(1)).then(function(result){
            $scope.items = result.data;
        });
    };

    $scope.readBlog = function () {
        blogPosts.getPost($routeParams.slug).then(function (response) {
            $scope.item = response.data;
        });
    }
    
}]);