var userManagerModule = angular.module('userManagerModule', ['ui.bootstrap', 'authService','userService'])

userManagerModule.controller('UserManagerCtrl', function($scope, $document, $uibModal, $location, auth, account, $routeParams) {


$scope.getUsers = function () {
      account.getUsers().then(function (result) {
          $scope.users = result.data.data.data;
      });
  }

  $scope.registerAccount = function () {
      var credentials = {
        name: $scope.register.username,
        email: $scope.register.email,
        password: $scope.register.password,
        password_confirmation: $scope.register.password,
        role: $scope.register.role
      };

      account.register(credentials).then(function (response) {
        if(response.success == true){
          toastr.success("Account has been created!.", "Account creation successful.", {timeout: 3000})
        }
      })


  }

  $scope.singleUser = function (id) {
      $location.path('/manage-users/'+id)
    };

  $scope.updateAccount = function () {
    account.getSingleUser($routeParams.id).then(function(result){
        $scope.user  = result.data.data;
      })
  }


  $scope.banUser = function (id) {
    account.banUser(id).then(function(response){
      console.log("response: "+ JSON.stringify(response));
    })
  }

  $scope.unbanUser = function (id) {
    account.unbanUser(id).then(function(response){
      console.log("response: "+ JSON.stringify(response));
    })
  }


  $scope.status = {};






  $scope.open = {
    openModal : function(template){
      var parentElem = $document.find('body')
      $uibModal.open({
        templateUrl: 'partials/' + template + '.html',
        appendTo: parentElem,
        scope: $scope
      })
    },

    init : function(template, id) {
      $scope.userID = id
      $scope.userData = $scope.users.filter(function(elt){
        return elt.id == id
      })[0]
      $scope.newdata = {}
      $scope.newdata = Object.create($scope.userData)
      console.log("UserData: "+ JSON.stringify($scope.userData))
      this.openModal(template)
    },

    banUser: function (id) {
      console.log("ID: "+ id)
      account.banUser(id).then(function (response) {      
        console.log("response: "+ JSON.stringify(response));
      })
    },

    unbanUser: function (id) {
      account.unbanUser(id).then(function (response) {
        // body...
      })
    },
  }

})
