var packageModule = angular.module('packageModule', ['ui.bootstrap', 'authService'])

packageModule.controller('PackageCtrl', function($scope, $document, $uibModal, $location, auth) {

  if(auth.isAuthed()){
      $scope.packages = [
          {
              id: 0,
              name: 'Standard',
              price: 0,
              desc: 'Limited',
              duration: 1
          },
          {
              id: 1,
              name: 'Premium',
              price: 35000,
              desc: 'Multiple events',
              duration: 1
          },
          {
              id: 2,
              name: 'Premium',
              price: 87500,
              desc: 'Multiple events',
              duration: 4
          },
          {
              id: 3,
              name: 'Premium',
              price: 175000,
              desc: 'Multiple events',
              duration: 6
          }
      ]
  }
  else{
    $location.path('home');
  }

  $scope.open = {

    openModal : function(template){
      var parentElem = $document.find('body')
      $uibModal.open({
        templateUrl: 'partials/' + template + '.html',
        appendTo: parentElem,
        scope: $scope
      })
    },

    init : function(template, id) {
      $scope.packageId = id
      $scope.packageData = $scope.packages.filter(function(elt){
        return elt.id == id
      })[0]
      $scope.newdata = {}
      $scope.newdata = Object.create($scope.packageData)
      this.openModal(template)
    },

    save : function(id){
      for (i = 0; i < $scope.packages.length; i++){
        if ($scope.packages[i].id == id){
          $scope.packages[i] = $scope.newdata
        }
      }
      return true
    },

    delete : function () {
      for (var i = 0; i < $scope.packages.length; i++){
        if ($scope.packages[i].id == $scope.packageId ){
          $scope.packages.splice(i, 1)
        }
      }
      return true
    }
  }

})
